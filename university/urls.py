
from django.urls import path
from university.views import *


urlpatterns = [
   path('universities/', UniversityLists.as_view()),
   path('universities/<int:pk>/', UniversityDetail.as_view(), name='detail'),
   path('programs/', ProgramList.as_view()),
   path('programs/<int:pk>/', ProgramDetail.as_view()),
   path('departments', DepartmentList.as_view()),
   path('Departments/<int:pk>/', DepartmentDetail.as_view()),
   path('Facults/',FacultLIst.as_view()),
   path('Facults/<int:pk>/',FacultDetail.as_view()),






]
